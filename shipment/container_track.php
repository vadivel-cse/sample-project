
<!DOCTYPE html>
<html>
<head>
<title>QMS shipment track</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="style.css" />
</head>
<body>

<?php
	set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
	include 'PHPExcel/IOFactory.php';
	$messageSuccess = '';
	$messageDanger = '';
	$target_dir = "uploads/";
	$time = date("Y-m-d-H-i-s");	
if(isset($_POST["submit"])) {	
	$file_iput = $target_dir . basename($time."-".$_FILES["shipment_file"]["name"]);
	$FileType = strtolower(pathinfo($file_iput,PATHINFO_EXTENSION));		
	if($FileType == 'xlsx'){
		if (move_uploaded_file($_FILES["shipment_file"]["tmp_name"], $file_iput)) {
			$target_file = $file_iput;
			$messageSuccess = "File has been uploaded.";
			excelRead($target_file); 			
		} else {
			$messageDanger = "Sorry, there was an error uploading your file.";
		}
    }else{
		$messageDanger = "Sorry , can't upload this file please choose xlsx file only";
	}	
}

function excelRead($target_file){
	try {
		$objPHPExcel = PHPExcel_IOFactory::load($target_file);
	} catch(Exception $e) {
		die('Error loading file "'.pathinfo($target_file,PATHINFO_BASENAME).'": '.$e->getMessage());
	}
	$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
	$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet
	filterData($allDataInSheet, $target_file);	
}

function filterData($allDataInSheet, $target_file){
	$trackid = $allDataInSheet;
	$array = array();
	foreach($trackid as $code){
		$filter = substr($code['B'], 0, 4);
		if($filter == 'OOLU' || $filter == 'OOCU'){
			$array[] = update_oolu($code['B'], $target_file);
		}
		if($filter == 'ECMU'){
			$array[] = update_ecmu($code['B'], $target_file);
		}
		if($filter == 'CMAU'){
			$array[] = update_cmau($code['B'], $target_file);
		}
		if($filter == 'MRSU'){
			$array[] = update_mrsu($code['B'], $target_file);
		}		
	}	
	excelWrite($array, $target_file);
}

function update_oolu($code, $target_file){
	
	$url = 'https://moc.oocl.com/party/cargotracking/mobile/mobile_ct_search.jsf;jsessionid=e20c07cc4862edb3c64a9b8bf5e8ee65905b3d76a500b49405fdae7830760f44.e38NbxiLa3qOci0Oa3eQaxaMaO0?ANONYMOUS_TOKEN=jatKGXCQadPuRgqBvrQxMCCOOCL&ENTRY=MCC&ENTRY_TYPE=OOCL&PREFER_LANGUAGE=en-US&isFromMobile=true&isFromAppleApp=false&deviceType=';

	$fields_string = "form%3AlanguageChangeRequestPage=&form%3Abl_num=&form%3Abkg_num=&form%3Acntr_num=".$code."&form%3AgoToCargoTrackingCntr=Search&form%3AmobileLanguage=en-US&form%3AcurrentSearchType=&form_SUBMIT=1&form%3A_link_hidden_=&jsf_tree_64=H4sIAAAAAAAAAK1SzU4bMRAeImhoqRrxJ9RbJSq1qqrdB8iNlYBEW1GFgIRyiByvs7vJrm28Y0gu3Lj3AfoEVZ6gT8CNa1%2Bi1547VgJZQaTm0MvYM%2F6%2B%2BeazPfkNa4WBI2Vij2nGE%2BHl4z7joqBUZylnmCrptY0Qp2gsR2vEFyZZLMz7eTFQuVZSSDy82Pv1vfnnvgKrHXjZ5UmaRYbq0OyEJOFPJfyZhF%2BS8JeVqIdQ6%2FKHLMhYUSBshQN2xfyMydgnfCpjgr2ewxrRJdxApQPVrlNGomx3SpyT3kBwrI%2B0NbNR%2F8dt1H9OPtxt3n77UQEYaQBYKdwYYF1co3xjmmuET26WkTfVehzbSzDPvGMK1DNnMgpTOUSocKlnTBdfUWWg9dK9GlJbPE6jyD3MW%2BffkoMgoY1oiUsrCvxKhXnDj8s0bIsRIrzoZV1p8zn383LGDiyionG2Y9VWATO0GMaH9JAHYdnrFkK1N4zLGi7uIuw8Zw7jJ9R1LtEs4O4%2B4wYELINqCG9y1UszEc7u68npJrfG%2FfRTwQxP2mMtyPz%2BP8wfKpMjrPZpIfS7xeizxnkqrltKodZ%2FAZdbtAqvAwAA&jsf_state_64=H4sIAAAAAAAAAN2YW4wTVRjHv%2B0u7AIrdwh36qIumKV76%2B4WFsJuW2AL3UtoQQShTKdn2%2B5OZ4aZ0%2B0syGZR0UQSxSioCUYSNfEBffBFw5OXB6PxEkh8MTESQyQmXhJfUB%2FUc8603XY6szvbgg%2FOw2l75jtnvvP9v%2B93zvTqLzAnrcCyo8ERboxzCZwYdw1GRxCPuy98dfj1xeoWwQGgyQBQnT4JE0CvOflvtTK5VAWWsNFpnBRcfZya6OfkObXffvTJyhPXq8GxB%2BYLEhfbw%2FFYUgIwDycUpCYkIabJu3rYNPWZOtIupl81Nt%2FiqfmCEs8JaOKPJScut%2Fz5swNqAlCXIM%2FgpRgKQi0vpUWsjGNYylbQTFfQHMJKUox3B6GO%2FkxzcUQdriXmY5yS5ETMfmryP%2BTC4DgYIg0SMQCG%2BX3h%2FmDE2xsK%2BDA83MxzSlzCCsePkgmbU1I0KaDsR4THERVxCp9wjajU6YVTIQxK5PHnb1344vlN3zugah%2FMGeOENNJyK2NGA%2BlUFClPX724fsFLN8%2FnwkyvFXS6ZVMx6FUUbjyYVLF29sb6Vz%2FlXquGqgDUqMlTiI2pytTQNi%2FLlFQrp6TCUDMsKSkZ6z5ormGOR6prD%2BmjNitV2taUKFKNIVBoz0spWRJJtFwHA3SsS1LiLk7m%2BARypcZ1m76A3797IOIb7O%2FvHfBHAgNDB8OhSGh3uDRVQghf85%2B7eOmD993VNAKZevrcrBdVRFbq8%2FaIkBRHI4lkLIbEiIZhFfHCpaZF5pOAsOrq7z2wPxLwY6hzuz3uWFtnp5lV79BQMLDbT9xoLrmXTMmCy4%2BGubSA9%2BidD%2FTKsjAelkaR2Hf5a3%2B3eOlKPXOyG1rNU2Nw0BcsyA%2BMyKQcRjRDyHK4j7vHrmuabHExBZaQdS9gei4w0XOdWRGS7OVFo65BEjEMtSx8vGghroN2r88HjW%2BjvzcyY6f1IqfWR4KYksSCMihcJIlyY0mUEaloFOf48X6EE1LMmxRjZOZs3leRWglCVQrDWlbNWjMSmnXD3ZpMqKEmJbFbs5w5zMWN1rmZqW4KrNYjREyNdu81f37%2B1h33cw5mtyxvN2XxxrlnQ78fubGDFSrxYGPOA5PZAkT27w5MeOofffE3%2BmQa0NZMCHZtOu2jEQ1nIzooksRGh5Io4%2BIThAkomOUVqax%2BFuUi8zOF%2BNAhZ4R3Fn3v3Dh0%2B6f1p%2FfmqSLLWuYpeGLWWevs6fA0tW7rcBJ6k4XtbKhwBQ0kMTBU3353Mpv0tEyLBm5uPJXY6hto3NJdXB0YFkXTGEuiMwd1vX8WVTIiy%2FTn5lxdjMj26iI2zOqCNk1atk63sraFte57Iy2durMy8TpbiHjuuyzejy%2B%2FZS3eCLd131Al4s0z6SPirc4N9LEHHkAn00jFQ3QmsksUkq%2BPbRMYNjCNLYfZkr69jS%2BUvmhMFRvj0PcpfYMnWHjIGnmHqImBePVZmpgOIzxjY6xwtiqPKYNZZv7k0Zsf%2Fv2CI8ueNvKEDQW8MphTXFVfezu68cHPXsnh6jDhf6N1vihTgXQW5a5%2BeqQZOwrJcjLW3dTa0uVk4ZwuYYscaNDYrpo%2FXjBPvJIkIE780qlMfnP5r1%2FJ3nIkdw6TIVvFj1VZpeHamRJyblSIiOmS81QYaRjDApZ7usVMidbVznV5ujweO4lGu8Ps5kHWPsLaI5kh2GEdKn179ilJjMjp10XiLgwOBzmahvoZ1ELASZiY9QHA2dNGBGzfNrOAM3uVVZWu8Jj%2BcTx%2FVCpSOiBiFEfK0h%2BuvHnn7DMeBz0dZ5WmoxaRyG0yO2ulyI3lliBaa76LLCOrkYoW5Q0aEeRlrCN0ZGlgMsBeTkSjleREC6zbdDqlxo828qmUS8dvNuqNx0yLlkS1oYAS7NYh%2BsYUFRB5p5ORQklRjC7zhRAiVpNHF%2FmzBuYyfyyyzQs99l%2B3nD2t7iZ3QZbReWm%2BaJkTcLycrO1oamvpKJrPIm70KSYngAB0zJTr3nHv9KVntu%2BfAq385cy870%2FjWUPhiwqGNQrCaUWkIUrGCK69Qd1us2Gvp2RkMSubrLXR0ThDK%2B1DGOp1mOqd9iqH4yqpnP3QZZ9bkjQ6E0hPw3g5GnrKA2mRQ9MxNIfAu8fG5aWoG42zp6bJCc4ChqNxe5rGYv8xDenAM1aAo92TtDlbAXS2VQgdP7TYKG3LDDXjTRrU8lcyC94YsrQIIGuNqNm%2F916xpo4XsVIIm%2Fuy%2F5jovTYz011JZg7AdtvFzZNTBUfuKXo8VAvgTMDjZYjY3tbU6m6fNXBKfLLDnNBdY86KEqL4iHg56Kwxhw41sact3%2FV%2Fo057e1NbayXU2QvtMxe4b7pMNQOPBmPlL8Y%2BeEr8MrDnfgN7DPazJpAFdxbqq8r9b8LSdZC8XLB0Lb5nL0%2BHOyvJ052wxTp0xe5YIEeAkXLk62ry2DjiGDywAoz98C%2Fh04qCRBxivoTH5bwCK3X%2BG2%2FbEwFtq0SEaf9ENELX6KBPillJ8yScLUcacvwsxERFjlkppmn%2FAkhksJKCHAAA&jsf_viewid=%2Fcargotracking%2Fmobile%2Fmobile_ct_search.jsp";

	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded','Connection: Keep-Alive'));
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36");
	curl_setopt($ch,CURLOPT_REFERER, "https://moc.oocl.com/party/cargotracking/mobile/mobile_ct_search.jsf?ANONYMOUS_TOKEN=zlquJrQmiAegJbVRLPVhMCCOOCL&ENTRY=MCC&ENTRY_TYPE=OOCL&PREFER_LANGUAGE=en-US&isFromMobile=true&isFromAppleApp=false&deviceType=");
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
	curl_setopt($ch,CURLOPT_POST, 12);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$result = curl_exec($ch);
	curl_close($ch);

	if($result != '/'){
		$eventValue = explode('class="bd"', $result);
		preg_match_all('/<br>.+/', $eventValue[1], $getValue);
		$details = $getValue[0][1];
	}else{
		$details = 'Not Found';
	}
    $website = 'https://moc.oocl.com';
	$result = array('Website'		=> $website,
					'Code'			=> $code,
					'Description' 	=> $details
					);
	return $result;
	
	
}
function update_ecmu($code, $target_file){
	$website = 'www.google.com';
	$details = 'Not Found';
	$result = array('Website'		=> $website,
					'Code'			=> $code,
					'Description' 	=> $details
					);
	return $result;
}

function update_mrsu($code, $target_file){
	$data = file_get_contents('https://www.safmarine.com/how-to-ship/tracking?trackingNumber='.$code.'');
	$dataS = file_get_contents('https://my.safmarine.com/tracking/search?searchNumber='.$code.'');
	preg_match_all('#<tr[^>]*>(.*?)</tr[^>]*>#is', $data, $getValue1);
	if(isset($getValue1[1][2])){	
		$getValue2 = $getValue1[1][2];
		$getValue3 = preg_replace('#<img[^>]*>#i', '', $getValue2);
		$details = preg_replace( "/\s+/", " ", $getValue3 );
	}else{
		preg_match_all('#<tr[^>]*>(.*?)</tr[^>]*>#is', $dataS, $getValue1);
		$getValue2 = $getValue1[0][0];
		$details = preg_replace( "/\s+/", " ", $getValue2 );	
	}	
	$website = 'https://www.safmarine.com';
	$result = array('Website'		=> $website,
					'Code'			=> $code,
					'Description' 	=> strip_tags($details)
					);
	return $result;
}

function update_cmau($code, $target_file){
	$website = 'http://www.cma-cgm.com';
	$data = file_get_contents('http://www.cma-cgm.com/ebusiness/tracking/search?SearchBy=Container&Reference='.$code.'');
	$exp = explode('class="date-current"', $data);
	$exp = explode('data-move="status', $exp[1]);
	preg_match('/(.+\>)(.+)(\<.+)/', $exp[1], $matches);	
	$cmauData = array($code, $matches[2]);	
	$result = array('Website'		=> $website,
					'Code'			=> $code,
					'Description' 	=> $cmauData[1]
					);
	return $result;
		
}

 function excelWrite($trackid, $target_file){
	try {
		$objPHPExcel = PHPExcel_IOFactory::load($target_file);
	} catch(Exception $e) {
		die('Error loading file "'.pathinfo($target_file,PATHINFO_BASENAME).'": '.$e->getMessage());
	}	

	$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);	
 	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth("30");
 	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth("30");	
 	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth("30");
 	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth("30");	
	
	$i = 1;
	foreach($trackid as $track){ 
        $i++;	
		$objPHPExcel->getActiveSheet(1)->setCellValue('A1', 'Website');
		$objPHPExcel->getActiveSheet(1)->setCellValue('B1', 'Shipment Track ID');
		$objPHPExcel->getActiveSheet(1)->setCellValue('C1', 'Details');
		$objPHPExcel->getActiveSheet(1)->setCellValue('D1', 'Date');
		$objPHPExcel->getActiveSheet(1)->setCellValue('A'.$i, $track['Website']);	
		$objPHPExcel->getActiveSheet(1)->setCellValue('B'.$i, $track['Code']);		
		$objPHPExcel->getActiveSheet(1)->setCellValue('C'.$i, $track['Description']);	
		$objPHPExcel->getActiveSheet(1)->setCellValue('D'.$i, date("Y-m-d H:i:s"));		
	} 				
				
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save($target_file);
} 

?>

	<header class="site_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center logo">
					<a href=""><img src="images/logo.jpg" alt="QMS Logo"></a>
				</div>
			</div>
		</div>
	</header>
	<!--header-->
	<section class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="fw uploadcol text-center">
						<?php if($messageDanger){ ?>
							<div class="alert alert-danger">
							  <strong>Error!</strong> <?php echo $messageDanger; ?>
							</div>
						<?php } ?>
						<?php if($messageSuccess){ ?>
						    <button onclick="window.history.go(-1)" class="btn-danger goback">Go Back</button>
							<div class="alert alert-success rbg">
							  <strong>Success!</strong> <?php echo $messageSuccess; ?>
							</div>
							<div class="fw text-center">
							<?php if(!empty($target_file)) { ?>
								<a href="<?php echo $target_file ?>"><img src="images/download.svg" class="download"><span class="blue_btn">Download Shipments Track Report</span></a>
							<?php } ?>					
							</div>						
						<?php } ?>						
						<div class="fw form_layout <?php if($messageSuccess){echo "hide";} ?>">					
							<h2>Shipments Track</h2>
							<form id="uploadForm" action="" method="post" enctype="multipart/form-data">            
								<div class="form-group files">
									<label>Upload Excel File </label>
									<input type="file" name="shipment_file" id="shipment_file" class="form-control">
								</div> 
								<input type="submit" value="Upload" id="submit" class="btn btn-danger submit" name="submit">							
							</form>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
<div id="preloader" style="display:none;">
  <div id="status">&nbsp;</div>
</div>
	
<script>	

</script>	
</body>
</html>
